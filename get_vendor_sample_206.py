import pandas as pd

file_name = 'products_general_09062022' #название импортируемого файла

df = pd.read_csv(f'{file_name}.csv', sep=';', dtype=str)


def get_vendor_sample_206(df=df, file_name=file_name):

    '''
    Функция принимает на вход выгрузку из админки cs-cart с произвольным количеством полей в виде csv-файла,
    И возвращает выборку товаров Огого с тем же количеством полей и в том же порядке, что и на входе,
    Полученный результат сохраняет в csv-файл
    '''

    def check_product_code(st):
        # Проверяет совпадения по полю Product Code
        if str(st).startswith('206'): return st

    df['Product code'] = df['Product code'].apply(lambda x: check_product_code(x))
    df = df[~df['Product code'].isna()].sort_values(by='Product code').reset_index(drop=True)
    df.to_csv(f'206_sample_{file_name}.csv', sep=';', index=False)

    print('DONE')

get_vendor_sample_206(file_name='products_general_09062022')
